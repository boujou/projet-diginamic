const express = require("express");
const cookieParser = require("cookie-parser");
const userRouter = require("./routes/user.routes");
const postRouter = require("./routes/post.routes");
require("dotenv").config({ path: "./config/.env" });
require("./config/db");
const { checkUser, requireAuth } = require("./middleware/auth.middleware");
const cors = require("cors");

const app = express();

const corsOptions = {
  origin: process.env.CLIENT_URL,
  credentials: true,
  allowedHeaders: ["sessionId", "Content-Type"],
  exposedHeaders: ["sessionId"],
  methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
  preflightContinue: false,
};

const port = process.env.PORT || 8000;

app.use(cors(corsOptions));

// middleware
app.use(cookieParser());
app.use(express.json());

// jwt
app.get("*", checkUser);
app.get("/jwtid", requireAuth, (req, res) => {
  res.status(200).send(res.locals.user._id);
});

//routes
app.use("/api/user", userRouter);
app.use("/api/post", postRouter);

//server
app.listen(port, () => console.log(`Serveur connecté sur le port: ${port}`));
