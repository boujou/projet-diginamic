// signup errors
exports.signUpErrors = (err) => {
  let errors = {
    pseudo: "",
    email: "",
    password: "",
  };

  if (err.message.includes("pseudo"))
    errors.pseudo = "Le pseudo doit contenir 3 caracteres minimum";

  if (err.message.includes("email")) errors.email = "Email incorrect";

  if (err.message.includes("password"))
    errors.password = "Le mot de passe doit faire 6 caracteres minimum";

  if (err.code === 11000 && Object.keys(err.keyValue)[0].includes("pseudo"))
    errors.pseudo = "Ce pseudo est déja utilisé";

  if (err.code === 11000 && Object.keys(err.keyValue)[0].includes("email"))
    errors.email = "Cet email est déja enregistré";

  return errors;
};

// signin errors
exports.signInErrors = (err) => {
  let errors = {
    email: "",
    password: "",
  };

  if (err.message.includes("email"))
    errors.email = "L'addresse email est iconnue";

  if (err.message.includes("password"))
    errors.password = "Le mot de passe ne correspond pas";

  return errors;
};

// upload errors
exports.uploadErrors = (err) => {
  let errors = { format: "", maxSize: "" };

  if (err.message.includes("invalid file"))
    errors.format = "Format incompatible";

  if (err.message.includes("max size"))
    errors.maxSize = "Le fichier dépasse 5 Mo";

  return errors;
};
